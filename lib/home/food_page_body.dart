import 'package:finalproject/utils/colors.dart';
import 'package:finalproject/widgets/big_text.dart';
import 'package:finalproject/widgets/icon_and_text_widget.dart';
import 'package:finalproject/widgets/small_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class FoodPageBody extends StatefulWidget {
  const FoodPageBody({Key? key}) : super(key: key);

  @override
  _FoodPageBodyState createState() => _FoodPageBodyState();
}

class _FoodPageBodyState extends State<FoodPageBody> {
  PageController pageController =PageController(viewportFraction: 0.8);
  var _currPageValue =0.0;
  double _scaleFactor =0.8;
  @override
  void initState(){
    super.initState();
    pageController.addListener(() {
      setState(() {
        _currPageValue =pageController.page!;
        print("Current value is"+_currPageValue.toString());
      });
    });
  }

  @override
  void dispose(){
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.redAccent,
      height: 320,
      child: PageView.builder(
        controller: pageController,
          itemCount: 5,
          itemBuilder: (context,position){
        return _buildPageItem(position);
      }),
    );
  }
  Widget _buildPageItem(int index){
    Matrix4 matrix4=new Matrix4.identity();
    if(index==_currPageValue.floor()){
      var currScale=1-(_currPageValue-index)*(1-_scaleFactor);
      matrix4 =Matrix4.diagonal3Values(1, currScale, 1);

    }
    else if(index== _currPageValue.floor()+1){
      var currScale =_scaleFactor+(_currPageValue-index+1)*(1-_scaleFactor);
      matrix4 =Matrix4.diagonal3Values(1, currScale, 1);
    }
    return Transform(
      transform: matrix4,
      child: Stack(
        children: [
          Container(
            height: 220,
            margin: EdgeInsets.only(left: 10,right: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: index.isEven?Color(0xFFFAA961):Color(0xFF9294cc),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(

                        "assets/image/fooodbanner1.png"
                    )
                )
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 120,
              margin: EdgeInsets.only(left: 30,right: 30,bottom: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,

              ),
              child: Container(
                padding: EdgeInsets.only(top: 10,left: 15,right: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BigText(text: "Gong Cha",color: AppColors.back),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Wrap(
                          children: List.generate(5, (index) {return Icon(Icons.star,color: AppColors.mainColor,size: 15,);}),
                        ),
                        SizedBox(width: 10,),
                        SmallText(text: "4.5",color: AppColors.grey),
                        SizedBox(width: 10,),
                        SmallText(text: "9988",color: AppColors.grey),
                        SizedBox(width: 10,),
                        SmallText(text: "review",color: AppColors.grey),

                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        IconAndTextWidget(iconData: Icons.circle_sharp,
                            text: "Yêu thích",

                            iconColor: AppColors.iconColor1),
                        IconAndTextWidget(iconData: Icons.location_on,
                            text: "0.8 km",

                            iconColor: AppColors.mint),
                        IconAndTextWidget(iconData: Icons.access_time_rounded,
                            text: "3km",

                            iconColor: AppColors.purple),


                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
