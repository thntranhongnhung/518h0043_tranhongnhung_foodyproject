import 'dart:ui';
class AppColors{
  static final Color textColor = const  Color(0xFFccc7c5);
  static final Color mainColor = const  Color(0xFFFAA961);
  static final Color iconColor1 = const  Color(0xFFBA633D);
  static final Color back = const  Color(0xFF2b1503);
  static final Color grey = const  Color(0xFF696969);
  static final Color mint = const  Color(0xFF9dd4ce);
  static final Color purple = const  Color(0xFFb2a1e6);

}