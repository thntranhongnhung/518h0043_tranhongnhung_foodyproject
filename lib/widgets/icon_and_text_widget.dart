import 'package:finalproject/widgets/small_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:finalproject/utils/colors.dart';
class IconAndTextWidget extends StatelessWidget {
  final IconData iconData;
  final String text;

  final Color iconColor;
  const IconAndTextWidget({Key? key,
    required this.iconData,
    required this.text,

    required this.iconColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(iconData,color: iconColor,),
        SizedBox(width: 5,),
        SmallText(text: text,color: AppColors.iconColor1),
      ],
    );
  }
}
